# Pyramid SAML

This is a small library using [OneLogin's SAML Python Toolkit](https://github.com/onelogin/python3-saml) to provide authentication and authorization via SAML for the
[Pyramid Web Framework](https://docs.pylonsproject.org/projects/pyramid/en/latest/).

For futher information, please refer to the [documentation](https://geo-bl-ch.gitlab.io/pyramid_saml/latest/).
