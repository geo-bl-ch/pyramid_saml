FROM python:3.11-slim AS builder

ADD . /src/

WORKDIR /src

RUN apt-get update && \
    apt-get install -y \
        build-essential \
        pkg-config \
        python3-dev \
        libxmlsec1-dev \
        libxslt1-dev \
        libxml2-dev && \
    make build

FROM python:3.11-slim

COPY --from=builder /src/dist/*.whl /tmp/
ADD demo /app

WORKDIR /app

RUN apt-get update && \
    apt-get install -y \
        pkg-config \
        build-essential \
        libxmlsec1-dev \
        libxslt1-dev \
        libxml2-dev && \
    python3 -m venv .venv && \
    .venv/bin/pip3 install /tmp/*.whl && \
    .venv/bin/pip3 install -e . && \
    apt-get purge -y \
        build-essential \
        libxmlsec1-dev \
        libxslt1-dev \
        libxml2-dev && \
    apt-get autoremove -y && \
    apt-get install -y \
        uwsgi-plugin-python3 \
        libxmlsec1 \
        libxmlsec1-openssl \
        libxslt1.1 \
        libxml2 && \
    adduser --uid 1001 --ingroup root --shell /bin/bash --disabled-password app && \
    chgrp -R 0 /etc/passwd && \
    chmod -R g=u /etc/passwd && \
    chown -R 1001:0 /app && \
    chmod -R g=u /app

COPY --chown=1001:0 uid_entrypoint.sh /usr/local/bin/

USER 1001

EXPOSE 8080

ENV PYRAMID_SAML_PATH=/app \
    PYRAMID_SAML_LOG_LEVEL=debug \
    PYRAMID_SAML_DEMO_PROTOCOL=http

ENTRYPOINT ["/usr/local/bin/uid_entrypoint.sh"]

CMD ["/app/.venv/bin/uwsgi", "--plugin", "python3", "--http-socket", "0.0.0.0:8080", "--ini-paste-logged", "/app/demo.ini"]
