# -*- coding: utf-8 -*-
import logging
from pyramid_saml import SAML_NAME_ID, SAML_SESSION_INDEX, SAML_USER_DATA, \
    SAML_ERROR


log = logging.getLogger('pyramid_saml_demo')


class Index(object):

    def __init__(self, request):
        self._request = request

    def render(self):
        session = self._request.session

        if SAML_SESSION_INDEX in session and \
                SAML_NAME_ID in session and \
                SAML_USER_DATA in session:
            loggedin = True
            user_name = session[SAML_NAME_ID]
            user_data = session[SAML_USER_DATA]
        else:
            loggedin = False
            user_name = None
            user_data = {}

        if SAML_ERROR in session:
            error = session[SAML_ERROR]
        else:
            error = None

        return {
            'loggedin': loggedin,
            'user_name': user_name,
            'user_data': user_data,
            'error': error
        }
