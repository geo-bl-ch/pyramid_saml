# -*- coding: utf-8 -*-
import logging
import os
from pyramid.config import Configurator
from pyramid.session import SignedCookieSessionFactory
from pyramid_mako import add_mako_renderer


log = logging.getLogger('pyramid_saml_demo')


def main(global_config, **settings):

    # Set log level through env variable
    log_level = '{0}'.format(os.environ.get('PYRAMID_SAML_LOG_LEVEL')).lower()
    if log_level == 'error':
        log.setLevel(logging.ERROR)
    elif log_level == 'warning':
        log.setLevel(logging.WARNING)
    elif log_level == 'debug':
        log.setLevel(logging.DEBUG)
    else:
        log.setLevel(logging.INFO)

    try:
        config = Configurator(settings=settings)
        config.set_session_factory(
            SignedCookieSessionFactory('pyramidsamldemo')
        )
        add_mako_renderer(config, ".html")
        config.include('pyramid_mako')
        config.include('pyramid_saml', route_prefix='auth')
        config.include('pyramid_saml_demo.routes')
        config.scan()
    except Exception as e:
        log.exception(e)

    return config.make_wsgi_app()
