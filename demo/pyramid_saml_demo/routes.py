# -*- coding: utf-8 -*-
from pyramid_saml_demo.views.index import Index


def includeme(config):

    config.add_route('index', '/')
    config.add_view(
        Index,
        attr='render',
        route_name='index',
        renderer='pyramid_saml_demo:templates/index.html',
        request_method='GET'
    )
