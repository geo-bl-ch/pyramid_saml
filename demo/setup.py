# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

requires = [
    'uwsgi',
    'pastescript',
    'pyramid',
    'pyramid_mako',
    'pyramid_saml'
]

setup(
    name='pyramid_saml_demo',
    version='0.0.1',
    description='Pyramid SAML Demo',
    long_description='Pyramid SAML demo application.',
    author='Karsten Deininger',
    author_email='karsten.deininger@bl.ch',
    url='https://gitlab.com/geo-bl-ch/pyramid_saml',
    install_requires=requires,
    packages=find_packages(),
    include_package_data=True,
    entry_points={
        'paste.app_factory': [
            'main = pyramid_saml_demo:main'
        ]
    }
)
