Changelog
=========

0.0.2
-----

- Overwrite SAML request parameters using environment variables
- Drop support for Python 3.6
- Add support for Python 3.10

0.0.1
-----

- Initial version
