#!/bin/bash

set -e

if curl -f -o public.zip "$CI_PAGES_URL/public.zip"; then
    unzip public.zip
    rm -f public.zip
else
    mkdir -p public
fi

echo "Public before:"
ls -ls public

if  [[ -z "${CI_COMMIT_TAG}" ]]; then
    mkdir -p public/master
    rm -rf public/master/*
    PYRAMID_SAML_VERSION="master" make doc
    cp -r doc/build/html/* public/master/
else
    mkdir -p public/${CI_COMMIT_TAG}
    mkdir -p public/latest
    rm -rf public/${CI_COMMIT_TAG}/*
    rm -rf public/latest/*
    PYRAMID_SAML_VERSION="${CI_COMMIT_TAG}" make doc
    cp -r doc/build/html/* public/${CI_COMMIT_TAG}/
    cp -r doc/build/html/* public/latest/
fi

cd public
tree -H '.' -L 1 -d --noreport --charset utf-8 > index.html
cd ..

zip -r public.zip public
cp -f public.zip public/

echo "Public after:"
ls -ls public
