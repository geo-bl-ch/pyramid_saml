Pyramid SAML
============

.. toctree::
   :maxdepth: 2
   :hidden:

   pyramid_saml
   usage
   changelog.md

This is a small library using `OneLogin's SAML Python Toolkit <https://github.com/onelogin/python3-saml>`__
to provide authentication and authorization via SAML for the
`Pyramid Web Framework <https://docs.pylonsproject.org/projects/pyramid/en/latest/>`__.

.. note:: The latest development builds are available on https://test.pypi.org/project/pyramid-saml/.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
