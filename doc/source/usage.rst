Usage
=====


Requirements
------------

In order to use this library, you need an existing Pyramid application with
a configured Session Factory. For example, you can add a
:code:`SignedCookieSessionFactory` as follows.

.. code-block:: python3

    from pyramid.session import SignedCookieSessionFactory

    ...

    def main(global_config, **settings):
        config = Configurator(settings=settings)
        config.set_session_factory(
            SignedCookieSessionFactory('myappusingsaml')
        )


Installation
------------

The common way to install this library, is using *pip* to download the required
packages from the Python Package Index.

.. code-block:: shell

    pip install pyramid_saml

You can also build it from source or use the latest development version from
`test.pypi.org <https://test.pypi.org/project/pyramid-saml/>`__.

After the package has been installed, you can include in your application using
the :code:`include` function.

.. code-block:: python3

    config.include('pyramid_saml')

This will add five routes to your application: :code:`/sso`, :code:`/slo`,
:code:`/acs`, :code:`/sls` and :code:`/metadata`. Alternatively, you can add a
route prefix to separate these routes from your existing ones.

.. code-block:: python3

    config.include('pyramid_saml', route_prefix='auth')


Configuration
-------------

First you should specify a *SAML path*. The library expects the configuration
files and certificates to be found in this folder. This can be done either by
passing :code:`pyramid_saml.saml_path` in the applications configuration, e.g.
in the INI file, or by setting the environment variable
:code:`PYRAMID_SAML_PATH`. The environment variable always overwrites the
value from the application configuration.

Configure SAML als described for
`OneLogin's SAML Python Toolkit <https://github.com/onelogin/python3-saml>`__,
using the *settings.json* and maybe also *advanced_settings.json* files.
The contents are added to the application configuration under the key
:code:`pyramid_saml` and can be accessed e.g. using
:code:`config.get_settings()` or :code:`request.registry.settings`.

Additionally, you can also specify an URL to request the IdP metadata on
application start up. This can be done by adding a :code:`metadata_url`
in the configuration or by setting the environment variable
:code:`PYRAMID_SAML_METADATA_URL`. If the IdP returns multiple entities,
you can specify a certain entity by setting
:code:`PYRAMID_SAML_IDP_ENTITY_ID`.

If you are using ADFS as IdP, you should also set
:code:`"lowercase_urlencoding": false` in your configuration.

You can also have a look on the demo application for a configuration
example.
