Module *pyramid_saml*
=====================

.. automodule:: pyramid_saml
    :members:


Class *SAML*
------------

.. autoclass:: SAML
    :members:
    :show-inheritance:
