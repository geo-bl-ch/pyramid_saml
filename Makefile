.PHONY: clean
clean:
	rm -rf .venv
	rm -rf pyramid_saml.egg-info
	rm -rf .pytest_cache
	rm -rf test/__pycache__
	rm -f .coverage
	rm -rf doc/build/

.venv/.timestamp:
	python3 -m venv .venv
	touch $@

.venv/.requirements.timestamp: .venv/.timestamp requirements.txt setup.py
	.venv/bin/pip3 install -r requirements.txt

.PHONY: lint
lint: .venv/.requirements.timestamp
	.venv/bin/flake8

.PHONY: test
test: .venv/.requirements.timestamp
	.venv/bin/pytest

.PHONY: build
build: .venv/.requirements.timestamp
	.venv/bin/python setup.py clean check sdist bdist_wheel

.PHONY: doc
doc: .venv/.requirements.timestamp
	cp CHANGELOG.md doc/source/changelog.md
	.venv/bin/sphinx-build doc/source/ doc/build/html/
